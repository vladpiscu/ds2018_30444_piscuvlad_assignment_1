import service.CityService;
import service.FlightService;
import service.UserService;

public class ServiceFactory {
    private CityService cityService;
    private FlightService flightService;
    private UserService userService;

    private static ServiceFactory instance;

    public static ServiceFactory instance(){
        if(instance == null){
            instance = new ServiceFactory();
        }
        return instance;
    }

    private ServiceFactory() {
        cityService = new CityService(DaoFactory.instance().getCityDao());
        flightService = new FlightService(DaoFactory.instance().getFlightDao());
        userService = new UserService(DaoFactory.instance().getUserDao());
    }

    public CityService getCityService() {
        return cityService;
    }

    public FlightService getFlightService() {
        return flightService;
    }

    public UserService getUserService() {
        return userService;
    }
}
