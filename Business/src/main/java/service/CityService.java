package service;

import dao.CityDao;
import entity.City;

import java.util.List;

public class CityService {
    private CityDao cityDao;

    public CityService(CityDao cityDao) {
        this.cityDao = cityDao;
    }

    public City addCity(String name, String latitude, String longitude){
        City city = new City(name, latitude, longitude);
        return cityDao.addCity(city);
    }

    public City findCity(int id){
        return cityDao.findCityById(id);
    }

    public List<City> findCities(){
        return cityDao.findCities();
    }

    public boolean deleteCity(int id){
        return cityDao.deleteCity(id);
    }
}
