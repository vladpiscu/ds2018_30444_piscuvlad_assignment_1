package service;

import dao.FlightDao;
import entity.City;
import entity.Flight;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

public class FlightService {
    private FlightDao flightDao;

    public FlightService(FlightDao flightDao) {
        this.flightDao = flightDao;
    }

    public Flight addFlight(String flightNumber, String airplaneType, City departureCity, LocalDateTime departureTime, City arrivalCity, LocalDateTime arrivalTime) {
        Flight flight = new Flight(flightNumber, airplaneType, departureCity, Timestamp.valueOf(departureTime), arrivalCity, Timestamp.valueOf(arrivalTime));
        return flightDao.addFlight(flight);
    }

    public void updateFlight(int id, String flightNumber, String airplaneType, City departureCity, LocalDateTime departureTime, City arrivalCity, LocalDateTime arrivalTime) {
        Flight flight = new Flight(flightNumber, airplaneType, departureCity, Timestamp.valueOf(departureTime), arrivalCity, Timestamp.valueOf(arrivalTime));
        flight.setId(id);
        flightDao.updateFlight(flight);
    }

    public List<Flight> findFlights(){
        return flightDao.findFlights();
    }

    public boolean deleteFlight(int id){
        return flightDao.deleteFlight(id);
    }
}
