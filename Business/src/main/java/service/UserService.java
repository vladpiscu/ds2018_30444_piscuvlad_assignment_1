package service;

import dao.UserDao;
import entity.Role;
import entity.User;

public class UserService {
    private UserDao userDao;

    public UserService(UserDao userDao){
        this.userDao = userDao;
    }

    public User addUser(String username, String password, Role role){
        User user = new User(username, password, role);
        return userDao.addUser(user);
    }

    public User findByUsernameAndPassword(String username, String password){
        return userDao.findUserByUsernameAndPassword(username, password);
    }
}
