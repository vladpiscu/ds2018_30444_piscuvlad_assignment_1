import dao.CityDao;
import dao.FlightDao;
import dao.UserDao;
import entity.Flight;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DaoFactory {
    private CityDao cityDao;
    private FlightDao flightDao;
    private UserDao userDao;

    private static DaoFactory instance;

    public static DaoFactory instance(){
        if(instance == null){
            instance = new DaoFactory();
        }
        return instance;
    }

    private DaoFactory() {
        SessionFactory sessionFactory = new Configuration().configure().addAnnotatedClass(Flight.class).buildSessionFactory();
        cityDao = new CityDao(sessionFactory);
        flightDao = new FlightDao(sessionFactory);
        userDao = new UserDao(sessionFactory);
    }

    public CityDao getCityDao() {
        return cityDao;
    }

    public FlightDao getFlightDao() {
        return flightDao;
    }

    public UserDao getUserDao() {
        return userDao;
    }
}
