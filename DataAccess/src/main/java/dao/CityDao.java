package dao;

import entity.City;
import entity.Flight;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import java.util.List;

public class CityDao {
    private static final Log LOGGER = LogFactory.getLog(CityDao.class);

    private SessionFactory factory;

    public CityDao(SessionFactory factory) {
        this.factory = factory;
    }

    public City addCity(City city) {
        int cityId = -1;
        Transaction tx = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            cityId = (Integer) session.save(city);
            city.setId(cityId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return city;
    }

    @SuppressWarnings("unchecked")
    public List<City> findCities() {
        Transaction tx = null;
        List<City> cities = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            cities = session.createQuery("FROM City").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return cities;
    }

    @SuppressWarnings("unchecked")
    public City findCityById(int id) {
        Transaction tx = null;
        List<City> cities = null;
        Session session = factory.openSession();
        try  {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE id = :id");
            query.setParameter("id", id);
            cities = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return cities != null && !cities.isEmpty() ? cities.get(0) : null;
    }

    public boolean deleteCity(int id){
        Transaction tx = null;
        City city = findCityById(id);
        if(city == null)
            return false;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            session.delete(city);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
            return false;
        } finally {
            session.close();
        }
        return true;
    }
}
