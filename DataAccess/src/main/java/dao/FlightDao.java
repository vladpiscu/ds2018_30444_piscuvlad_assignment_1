package dao;

import entity.Flight;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import java.util.List;

public class FlightDao {
    private static final Log LOGGER = LogFactory.getLog(FlightDao.class);

    private SessionFactory factory;

    public FlightDao(SessionFactory factory) {
        this.factory = factory;
    }

    public Flight addFlight(Flight flight) {
        int flightId = -1;
        Transaction tx = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            flightId = (Integer) session.save(flight);
            flight.setId(flightId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flight;
    }

    public void updateFlight(Flight flight) {
        Transaction tx = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            session.update(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Flight> findFlights() {
        Transaction tx = null;
        List<Flight> flights = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            flights = session.createQuery("FROM Flight").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flights;
    }

    @SuppressWarnings("unchecked")
    public Flight findFlightById(int id) {
        Transaction tx = null;
        List<Flight> flights = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE id = :id");
            query.setParameter("id", id);
            flights = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flights != null && !flights.isEmpty() ? flights.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public Flight findFlightByFlightNumber(int flightNumber) {
        Transaction tx = null;
        List<Flight> flights = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE flightNumber = :flightNumber");
            query.setParameter("flightNumber", flightNumber);
            flights = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flights != null && !flights.isEmpty() ? flights.get(0) : null;
    }

    public boolean deleteFlight(int id) {
        Transaction tx = null;
        Flight flight = findFlightById(id);
        if (flight == null)
            return false;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            session.delete(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
            return false;
        } finally {
            session.close();
        }
        return true;
    }
}
