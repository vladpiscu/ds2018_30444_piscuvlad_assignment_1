package dao;

import entity.City;
import entity.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import java.util.List;

public class UserDao {
    private static final Log LOGGER = LogFactory.getLog(CityDao.class);

    private SessionFactory factory;

    public UserDao(SessionFactory factory) {
        this.factory = factory;
    }

    public User addUser(User user) {
        int userId = -1;
        Transaction tx = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            userId = (Integer) session.save(user);
            user.setId(userId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return user;
    }

    @SuppressWarnings("unchecked")
    public User findUserByUsernameAndPassword(String username, String password) {
        Transaction tx = null;
        List<User> users = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE (username = :username) AND (password = :password)");
            query.setParameter("username", username);
            query.setParameter("password", password);
            System.out.println(query.toString());
            users = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return users != null && !users.isEmpty() ? users.get(0) : null;
    }
}
