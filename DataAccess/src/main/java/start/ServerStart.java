package start;

import dao.FlightDao;
import dao.UserDao;
import entity.City;
import entity.Flight;
import entity.Role;
import entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class ServerStart {
    public static void main(String[] args){
        SessionFactory sessionFactory = new Configuration().configure().addAnnotatedClass(Flight.class).buildSessionFactory();
        FlightDao flightDao = new FlightDao(sessionFactory);
        City cityA = new City("CityA", "123.312", "321.123");
        City cityB = new City("CityB", "222.222", "333.333");
        Flight flight = new Flight("1234", "Boeing", cityA, Timestamp.valueOf(LocalDateTime.now()),
                cityB, Timestamp.valueOf(LocalDateTime.now()));
        flightDao.addFlight(flight);
        UserDao userDao = new UserDao(sessionFactory);
        User user = new User("a", "a", Role.ADMIN);
        userDao.addUser(user);
        System.out.println(flightDao.findFlightById(flight.getId()).getDepartureCity().getName());
        System.out.println(userDao.findUserByUsernameAndPassword("a", "a").getUsername());
    }
}
