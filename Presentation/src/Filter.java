import com.sun.deploy.net.HttpRequest;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "Filter")
public class Filter implements javax.servlet.Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) req;
        HttpServletResponse httpServletResponse = (HttpServletResponse) resp;
        HttpSession session = httpServletRequest.getSession();

        String requestPath = httpServletRequest.getRequestURI();

        if (session == null || (session.getAttribute("role") == null && !requestPath.equals("/login")) || (needsAuthentication(requestPath) && session.getAttribute("role").equals("client")) ) {
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/login");
        } else {
            chain.doFilter(req, resp); // Logged-in user found, so just continue request.
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

    //basic validation of pages that do not require authentication
    private boolean needsAuthentication(String url) {
        String[] validNonAuthenticationUrls =
                { "/login", "/userPage", "/allFlights" };
        for(String validUrl : validNonAuthenticationUrls) {
            if (url.endsWith(validUrl)) {
                return false;
            }
        }
        return true;
    }

}
