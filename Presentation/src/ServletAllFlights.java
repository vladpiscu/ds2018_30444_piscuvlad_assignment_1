import entity.City;
import entity.Flight;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

@WebServlet(name = "ServletAllFlights")
public class ServletAllFlights extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        List<Flight> flights = ServiceFactory.instance().getFlightService().findFlights();
        List<City> cities = ServiceFactory.instance().getCityService().findCities();
        String idValue = request.getParameter("searchCity");
        int id;
        if(idValue.equals(""))
            id = -1;
        else
            id = Integer.parseInt(idValue);
        City city = ServiceFactory.instance().getCityService().findCity(id);
        String result = "No result";
        if(city != null) {
            try {
                URL country = new URL("http://api.geonames.org/timezone?lat=" + city.getLatitude() + "&lng=" + city.getLongitude() + "&username=vladpiscu");
                URLConnection yc = country.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
                String inputLine;

                while ((inputLine = in.readLine()) != null)
                    if (inputLine.matches("<time>(.*)")) {
                        String[] results1 = inputLine.split(">");
                        String[] results2 = results1[1].split("<");
                        result = results2[0];
                    }
                in.close();
            }catch(Exception e){
                result = "Host can't be accessed!";
            }
        }
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>All Flights</title>");
            out.println("<style>");
            out.println("td { width: 200px; }");
            out.println("th { text-align: left; }");
            out.println("table, th, td { border-collapse: collapse; border: 1px solid black; }");
            out.println("</style>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>All Flights</h1>");
            ServletHelper.printFlightsTable(flights, out);
            out.println("</br>");
            out.println("<h1>All Cities</h1>");
            ServletHelper.printCitiesTable(cities, out);
            printCityForm(out);
            out.println("<a href=\"/userPage\"><b>Back</b></a>");
            out.println("<br/>");
            out.println("<h1>" + result + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        List<Flight> flights = ServiceFactory.instance().getFlightService().findFlights();
        List<City> cities = ServiceFactory.instance().getCityService().findCities();
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>All Flights</title>");
            out.println("<style>");
            out.println("td { width: 200px; }");
            out.println("th { text-align: left; }");
            out.println("table, th, td { border-collapse: collapse; border: 1px solid black; }");
            out.println("</style>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>All Flights</h1>");
            ServletHelper.printFlightsTable(flights, out);
            out.println("</br>");
            out.println("<h1>All Cities</h1>");
            ServletHelper.printCitiesTable(cities, out);
            printCityForm(out);
            out.println("<a href=\"/userPage\"><b>Back</b></a>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    private void printCityForm(PrintWriter out) {
        out.println("<hr/>");
        out.println("<form method=\"post\">");
        out.println("<legend><b>Find local time for city:</b></legend>");
        out.println("Search word: <input type=\"number\" name=\"searchCity\"/>");
        out.println("<input type=\"submit\" class=\"button\" name=\"action\" value=\"Search\" />");
        out.println("</form>");
    }


}
