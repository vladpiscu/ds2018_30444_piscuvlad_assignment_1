import entity.City;
import entity.Flight;
import service.CityService;
import service.FlightService;

import javax.ejb.Local;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@WebServlet(name = "ServletCrudFlights")
public class ServletCrudFlights extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idValue = request.getParameter("id");
        int id, departureCityId, arrivalCityId;
        LocalDateTime departureTime, arrivalTime;
        boolean creationError = false;
        boolean updateError = false;
        boolean deleteError = false;
        if(idValue.equals("")) {
            id = -1;
            updateError = true;
            deleteError = true;
        }
        else
            id = Integer.parseInt(idValue);
        String airplaneType = request.getParameter("airplaneType");
        String flightNumber = request.getParameter("flightNumber");
        String departureCityValue = request.getParameter("departureCity");
        if(departureCityValue.equals("")) {
            departureCityId = -1;
            creationError = true;
            updateError = true;
        }
        else
            departureCityId = Integer.parseInt(departureCityValue);
        String departureTimeValue = request.getParameter("departureTime");
        if(departureTimeValue.equals(""))
            departureTime = LocalDateTime.MIN;
        else
            departureTime = LocalDateTime.parse(departureTimeValue);
        String arrivalCityValue = request.getParameter("arrivalCity");
        if(arrivalCityValue.equals("")) {
            arrivalCityId = -1;
            creationError = true;
            updateError = true;
        }
        else
            arrivalCityId = Integer.parseInt(arrivalCityValue);
        String arrivalTimeValue = request.getParameter("arrivalTime");
        if(arrivalTimeValue.equals("")) {
            arrivalTime = LocalDateTime.MIN;
            creationError = true;
            updateError = true;
        }
        else
            arrivalTime = LocalDateTime.parse(arrivalTimeValue);
        String action = request.getParameter("action");
        FlightService flightService = ServiceFactory.instance().getFlightService();
        CityService cityService = ServiceFactory.instance().getCityService();
        City departureCity;
        City arrivalCity;
        String error = "";
        switch (action){
            case "Create":
                if(creationError)
                    error = "ERROR! You should complete the arrival and departure times and cities!";
                departureCity = cityService.findCity(departureCityId);
                arrivalCity = cityService.findCity(arrivalCityId);
                if(departureCity == null || arrivalCity == null)
                    error = "ERROR! You should complete the arrival and departure times and cities with valid values!";
                else
                    flightService.addFlight(flightNumber, airplaneType, departureCity, departureTime, arrivalCity, arrivalTime);
                break;
            case "Update":
                if(updateError)
                    error = "ERROR! You should complete the flight id and the arrival and departure times and cities!";
                departureCity = cityService.findCity(departureCityId);
                arrivalCity = cityService.findCity(arrivalCityId);
                if(departureCity == null || arrivalCity == null)
                    error = "ERROR! You should complete the arrival and departure times and cities with valid values!";
                else
                    flightService.updateFlight(id, flightNumber, airplaneType, departureCity, departureTime, arrivalCity, arrivalTime);
                break;
            case "Delete":
                if(deleteError)
                    error = "ERROR! You should complete the flight id!";
                flightService.deleteFlight(id);
                break;
        }
        printPage(response, error);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        printPage(response, "");
    }

    private void printPage(HttpServletResponse response, String error) throws IOException{
        response.setContentType("text/html;charset=UTF-8");
        List<Flight> flights = ServiceFactory.instance().getFlightService().findFlights();
        List<City> cities = ServiceFactory.instance().getCityService().findCities();
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>All Flights</title>");
            out.println("<style>");
            out.println("td { width: 200px; }");
            out.println("th { text-align: left; }");
            out.println("table, th, td { border-collapse: collapse; border: 1px solid black; }");
            out.println("</style>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>All Flights</h1>");
            ServletHelper.printFlightsTable(flights, out);
            out.println("</br>");
            out.println("<h1>All Cities</h1>");
            ServletHelper.printCitiesTable(cities, out);
            out.println("</br>");
            printFlightForm(out);
            out.println("<p><b>" + error + "</b></p>");
            out.println("<a href=\"/adminPage\"><b>Back</b></a>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    private void printFlightForm(PrintWriter out) {
        out.println("<hr/>");
        out.println("<form method=\"post\">");
        out.println("<legend><b>Make operations on flights:</b></legend>");
        out.println("Id: <input type=\"number\" name=\"id\"/>");
        out.println("<p>");
        out.println("Airplane type: <input type=\"text\" name=\"airplaneType\"/>");
        out.println("<p>");
        out.println("Flight number: <input type=\"text\" name=\"flightNumber\"/>");
        out.println("<p>");
        out.println("Departure city id: <input type=\"number\" name=\"departureCity\"/>");
        out.println("<p>");
        out.println("Departure time: <input type=\"datetime-local\" name=\"departureTime\"/>");
        out.println("<p>");
        out.println("Arrival city id: <input type=\"number\" name=\"arrivalCity\"/>");
        out.println("<p>");
        out.println("Arrival time: <input type=\"datetime-local\" name=\"arrivalTime\"/>");
        out.println("<p>");
        out.println("<input type=\"submit\" class=\"button\" name=\"action\" value=\"Create\" />");
        out.println("<input type=\"submit\" class=\"button\" name=\"action\" value=\"Update\" />");
        out.println("<input type=\"submit\" class=\"button\" name=\"action\" value=\"Delete\" />");
        out.println("</form>");
    }
}
