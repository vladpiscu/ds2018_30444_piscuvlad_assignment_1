import entity.City;
import entity.Flight;

import java.io.PrintWriter;
import java.util.List;

public class ServletHelper {
    public static void printFlightsTable(List<Flight> flights, PrintWriter out) {
        out.println("<table>");
        out.println("<body>");
        out.println("<thead>");
        out.println("<tr>");
        out.println("<th>Id</th>");
        out.println("<th>Airplane type</th>");
        out.println("<th>Flight number</th>");
        out.println("<th>Departure city</th>");
        out.println("<th>Departure time</th>");
        out.println("<th>Arrival city</th>");
        out.println("<th>Arrival time</th>");
        out.println("</tr>");
        out.println("</thead>");
        for(Flight flight : flights){
            out.println("<tr>");
            out.println("<th>" + flight.getId() + "</th>");
            out.println("<th>" + flight.getAirplaneType() + "</th>");
            out.println("<th>" + flight.getFlightNumber() + "</th>");
            out.println("<th>" + flight.getDepartureCity().getName() + "</th>");
            out.println("<th>" + flight.getDepartureTime().toString() + "</th>");
            out.println("<th>" + flight.getArrivalCity().getName() + "</th>");
            out.println("<th>" + flight.getArrivalTime().toString() + "</th>");
            out.println("</tr>");
        }
        out.println("<tbody>");
        out.println("</tbody>");
        out.println("</table>");
    }

    public static void printCitiesTable(List<City> cities, PrintWriter out) {
        out.println("<table>");
        out.println("<body>");
        out.println("<thead>");
        out.println("<tr>");
        out.println("<th>Id</th>");
        out.println("<th>Name</th>");
        out.println("<th>Longitude</th>");
        out.println("<th>Latitude</th>");
        out.println("</tr>");
        out.println("</thead>");
        for(City city : cities){
            out.println("<tr>");
            out.println("<th>" + city.getId() + "</th>");
            out.println("<th>" + city.getName() + "</th>");
            out.println("<th>" + city.getLongitude() + "</th>");
            out.println("<th>" + city.getLatitude() + "</th>");
            out.println("</tr>");
        }
        out.println("<tbody>");
        out.println("</tbody>");
        out.println("</table>");
    }
}
