import entity.Role;
import entity.User;
import start.ServerStart;

import java.io.IOException;
import javax.jms.Session;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class ServletLogin extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("uname");
        String password = request.getParameter("psw");
        HttpSession session = request.getSession();
        User user = ServiceFactory.instance()
                .getUserService()
                .findByUsernameAndPassword(username, password);
        if(user == null) {
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.html");
            requestDispatcher.forward(request, response);
        }
        else{
            if(user.getRole() == Role.ADMIN) {
                session.setAttribute("role", "admin");
                response.sendRedirect("/adminPage");
            }
            else {
                session.setAttribute("role", "client");
                response.sendRedirect("/userPage");
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiceFactory.instance();
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.html");
        requestDispatcher.forward(request, response);
    }
}
